<?php
	namespace knk\templater;

	class Templater
	{
		public static function generateContent($filePath,array $vars)
		{
			$fileContent = file_get_contents($filePath);

			foreach($vars as $varName => $var)
			{
				$fileContent = str_replace('{'.$varName.'}',$var,$fileContent);
			}

			return $fileContent;
		}
	};
?>

